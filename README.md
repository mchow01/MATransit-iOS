#Overview
Finds the closest MBTA subway (Red Line, Orange Line, Blue Line) or
Commuter Rail train station within 5 miles given a user's location;
provides real-time train schedule for each station.

#Keywords
mbta,subway,commuter rail,train,Boston,Massachusetts,public
transportation

#Download on the iTunes App Store
https://itunes.apple.com/us/app/matransit/id653572142?ls=1&mt=8

#Support URL
This

#Original Design Sketch via Penultimate and Evernote
https://www.evernote.com/shard/s31/sh/1019de89-3568-4296-894b-108d410573df/2c94202347c922c8d1cb37c34f63abb5
