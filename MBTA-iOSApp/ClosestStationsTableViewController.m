//
//  ClosestStationsTableViewController.m
//  MATransit
//

#import "ClosestStationsTableViewController.h"
#import "ScheduleTableViewController.h"

@implementation ClosestStationsTableViewController

@synthesize stations, tableView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Closest Stations";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [stations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Station";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:cellIdentifier];
    }
        
    // Set the text and subtext of the cell
    NSDictionary *elem = [stations objectAtIndex:indexPath.row];
    cell.textLabel.text = [elem objectForKey:@"stop_name"];
    double distance = [[elem objectForKey:@"distance"] doubleValue];
    NSString *subtext;
    if (distance > 1) {
        subtext = [NSString stringWithFormat:@"%f miles away", distance];
    }
    else {
        subtext = [NSString stringWithFormat:@"%f mile away", distance];
    }
    cell.detailTextLabel.text = subtext;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle: nil];
    ScheduleTableViewController *stvc = (ScheduleTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"scheduleTable"];
    stvc.stationInfo = [stations objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:stvc animated:true];
}

@end
