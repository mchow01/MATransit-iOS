//
//  GridTableViewCell.h
//  MATransit
//
//  Source: http://www.recursiveawesome.com/blog/2011/04/06/creating-a-grid-view-in-ios/
//

#import <UIKit/UIKit.h>

@interface GridTableViewCell : UITableViewCell

@property (nonatomic, strong) UIColor *gridLineColor;
@property (nonatomic) bool topCell;
@property (readonly) UILabel *cellLineColor;
@property (readonly) UILabel *cellDirection;
@property (readonly) UILabel *cellTimeRemaining;

@end
