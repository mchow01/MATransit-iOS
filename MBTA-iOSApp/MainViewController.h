//
//  MainViewController.h
//  MATransit
//

#import <UIKit/UIKit.h>
#import <MapKit/Mapkit.h>
#import <CoreLocation/CoreLocation.h>

@interface MainViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>
{
    NSString *myLocText;
    NSMutableArray *closestStations;
}
@property (strong, nonatomic) IBOutlet MKMapView *myMap;
@property (strong, nonatomic) IBOutlet UITextView *closestStatus;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLGeocoder *geocoder;
@property (nonatomic, retain) UIActivityIndicatorView *spinner;

-(void) getClosestStationWithLatitude:(double)latitude andLongitude:(double)longitude;
-(void) parseData:(NSData *)responseData;
-(void) displayClosestStationsTable;
-(void) displayStationAnnotations;
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation;
-(void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view;
@end
