//
//  StationAnnotation.h
//  MATransit-iOS
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface StationAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *subtitle;
@property (strong, nonatomic) NSDictionary *stationInfo;

- (id) initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates
                     title:(NSString *)paramTitle
                  subTitle:(NSString *)paramSubTitle
               stationInfo:(NSDictionary *)paramStationInfo;
@end
