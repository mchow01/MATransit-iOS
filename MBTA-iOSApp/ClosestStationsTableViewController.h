//
//  ClosestStationsTableViewController.h
//  MATransit
//

#import <UIKit/UIKit.h>

@interface ClosestStationsTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *stations;

@end
