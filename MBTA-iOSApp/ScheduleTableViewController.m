//
//  ScheduleTableViewController.m
//  MATransit
//

#import "ScheduleTableViewController.h"
#import "GridTableViewCell.h"

@implementation ScheduleTableViewController

@synthesize stationInfo, spinner;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    retrieved = false;
    return self;
}

- (void)viewDidLoad
{
    // Cells are not selectable: http://stackoverflow.com/questions/812426/uitableview-setting-some-cells-as-unselectable
    [super viewDidLoad];
    self.title = @"Upcoming Trains";
    
    // Turn off the tableView's default lines because we are drawing them all ourself
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // Call the schedule API
    NSString *addr = [NSString stringWithFormat:@"http://mbtamap.herokuapp.com/mapper/station_schedule.json?stop_name=%@", [self.stationInfo objectForKey:@"stop_name"]];
    // Ah ha! http://stackoverflow.com/questions/1441106/nsdata-nsurl-url-with-space-having-problem
    addr = [addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; // That's why stations with spaces didn't work!
    NSURL *url = [NSURL URLWithString:addr];
    
    // Initialize and start the spinner
    spinner = [[UIActivityIndicatorView alloc]  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.view.center; // http://stackoverflow.com/questions/8090579/how-to-display-activity-indicator-in-middle-of-the-iphone-screen
    spinner.color = [UIColor grayColor];
    [self.view addSubview:self.spinner];
    [spinner startAnimating];

    // Use a concurrent dispatch queue
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData* data = [NSData dataWithContentsOfURL:url];
        [self performSelectorOnMainThread:@selector(parseData:)
                               withObject:data
                            waitUntilDone:YES];
    });
}

-(void) parseData:(NSData *)responseData
{
    NSError* error;
    
    if (responseData != nil) {
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseData
                                                        options:0
                                                          error:&error];
        
        if (error == nil) {
            schedule = [NSArray arrayWithArray:json];
            [self.tableView reloadData];
        }
    }
    retrieved = true;
    [self.spinner stopAnimating];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([schedule count] == 0 && retrieved) {
        return [NSString stringWithFormat:@"%@: No trains", [self.stationInfo objectForKey:@"stop_name"]];
    }
    return [self.stationInfo objectForKey:@"stop_name"];
}

// See http://stackoverflow.com/questions/13659718/iphone-when-increasing-the-height-of-uitableview-section-header-the-height-of-th
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [schedule count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GridCell";

    GridTableViewCell *cell = (GridTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[GridTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.gridLineColor = [UIColor blackColor];
    }
    
    if (indexPath.row == 0) {
        cell.topCell = true;
    }
    else {
        cell.topCell = false;
    }
    
    NSDictionary *entry = [schedule objectAtIndex:indexPath.row];

    // Display proper line and color in cell background
    NSString *line = [entry objectForKey:@"line"];
    cell.cellLineColor.text = line;
    cell.cellLineColor.textColor = [UIColor whiteColor];
    if ([line isEqualToString:@"Red"]) {
        cell.cellLineColor.backgroundColor = [UIColor redColor];
    }
    else if ([line isEqualToString:@"Blue"]) {
        cell.cellLineColor.backgroundColor = [UIColor blueColor];
    }
    else if ([line isEqualToString:@"Orange"]) {
        cell.cellLineColor.backgroundColor= [UIColor orangeColor];
    }
    else if ([line isEqualToString:@"CR"]) {
        cell.cellLineColor.text = [entry objectForKey:@"trip"];
        cell.cellLineColor.backgroundColor= [UIColor purpleColor];
    }
    cell.cellDirection.text = [entry objectForKey:@"direction"];
    cell.cellTimeRemaining.text = [entry objectForKey:@"time_remaining"];
    return cell;
}

@end
