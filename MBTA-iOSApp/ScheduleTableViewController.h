//
//  ScheduleTableViewController.h
//  MATransit
//

#import <UIKit/UIKit.h>

@interface ScheduleTableViewController : UITableViewController
{
    NSArray *schedule;
    BOOL retrieved;
}

@property (strong, nonatomic) NSDictionary *stationInfo;
@property (nonatomic, retain) UIActivityIndicatorView *spinner;

@end
