//
//  GridTableViewCell.m
//  MATransit
//

#import "GridTableViewCell.h"

#define LINE_WIDTH 1
#define LINE_OFFSET 0.5
#define cell1Width 75
#define cell2Width 135
#define cellHeight 44

@implementation GridTableViewCell

@synthesize gridLineColor, topCell, cellDirection, cellLineColor, cellTimeRemaining;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
		topCell = false;
        
        // From http://stackoverflow.com/questions/5677716/how-to-get-the-screen-width-and-height-in-ios
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
		cellLineColor = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, cell1Width, cellHeight)];
		cellLineColor.textAlignment = UITextAlignmentCenter;
		cellLineColor.backgroundColor = [UIColor clearColor]; // Important to set or lines will not appear
		[self addSubview:cellLineColor];
        
		cellDirection = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width, 0, cell2Width, cellHeight)];
		cellDirection.textAlignment = UITextAlignmentCenter;
		cellDirection.backgroundColor = [UIColor clearColor]; // Important to set or lines will not appear
		[self addSubview:cellDirection];
        
        cellTimeRemaining = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width + cell2Width, 0, screenWidth - (cell1Width + cell2Width), cellHeight)];
		cellTimeRemaining.textAlignment = UITextAlignmentCenter;
		cellTimeRemaining.backgroundColor = [UIColor clearColor]; // Important to set or lines will not appear
		[self addSubview:cellTimeRemaining];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect
{
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, gridLineColor.CGColor);       
    
	CGContextSetLineWidth(context, LINE_WIDTH);
    
	// Add the vertical lines
	CGContextMoveToPoint(context, cell1Width + LINE_OFFSET, 0);
	CGContextAddLineToPoint(context, cell1Width + LINE_OFFSET, rect.size.height);
    
	CGContextMoveToPoint(context, cell1Width + cell2Width + LINE_OFFSET, 0);
	CGContextAddLineToPoint(context, cell1Width + cell2Width + LINE_OFFSET, rect.size.height);

	// Add bottom line
	CGContextMoveToPoint(context, 0, rect.size.height);
	CGContextAddLineToPoint(context, rect.size.width, rect.size.height - LINE_OFFSET);
    
	// If this is the topmost cell in the table, draw the line on top
	if (topCell) {
		CGContextMoveToPoint(context, 0, 0);
		CGContextAddLineToPoint(context, rect.size.width, 0);
	}
    
	// Draw the lines
	CGContextStrokePath(context);
}

- (void)setTopCell:(bool)newTopCell
{
	topCell = newTopCell;
	[self setNeedsDisplay];
}

@end
