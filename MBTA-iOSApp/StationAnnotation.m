//
//  StationAnnotation.m
//  MATransit-iOS
//

#import "StationAnnotation.h"

@implementation StationAnnotation

@synthesize coordinate, title, subtitle, stationInfo;

-(id) initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates title:(NSString *)paramTitle subTitle:(NSString *)paramSubTitle stationInfo:(NSDictionary *)paramStationInfo
{
    self = [super init];
    if (self != nil){
        coordinate = paramCoordinates;
        title = paramTitle;
        subtitle = paramSubTitle;
        stationInfo = paramStationInfo;
    }
    return(self);
}

@end
