//
//  MainViewController.m
//  MATransit
//

#import "MainViewController.h"
#import "ClosestStationsTableViewController.h"
#import "StationAnnotation.h"
#import "ScheduleTableViewController.h"

@implementation MainViewController

@synthesize myMap, locationManager, geocoder, closestStatus, spinner;

- (void)viewDidLoad
{
    [super viewDidLoad];
    closestStations = [[NSMutableArray alloc] init];
    
    // Set up map view
    self.myMap.mapType = MKMapTypeStandard;
    self.myMap.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.myMap.delegate = self; // necessary for delegates such as mapView:viewForAnnotation
    self.myMap.showsUserLocation = TRUE;
    
    // Set up location manager
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; // http://stackoverflow.com/questions/5930612/how-to-set-accuracy-and-distance-filter-when-using-mkmapview
        self.locationManager.distanceFilter = 400.0f; // 400 meters
        self.locationManager.headingFilter = kCLHeadingFilterNone;
        self.locationManager.purpose = @"To determine the closest train station near where you are.";
        [self.locationManager startUpdatingLocation];
    }
    else {
        UIAlertView *disabled = [[UIAlertView alloc]initWithTitle:@"Location Services Disabled"
                                                             message:@"Sorry, but location services are disabled. Please enable it"
                                                         delegate:self
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles:nil, nil];
        [disabled show];
    }
    
    // Initialize and start the spinner
    spinner = [[UIActivityIndicatorView alloc]  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.view.center; // http://stackoverflow.com/questions/8090579/how-to-display-activity-indicator-in-middle-of-the-iphone-screen
    spinner.color = [UIColor grayColor];
    [self.view addSubview:self.spinner];
    [spinner startAnimating];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.myMap = nil;
    self.geocoder = nil;
    self.locationManager = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // Get new location coordinates
    CLLocationCoordinate2D location =
    CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    // Reverse geocode latitude and longitude
    self.geocoder = [[CLGeocoder alloc] init];
    [self.geocoder reverseGeocodeLocation:newLocation
                        completionHandler:^(NSArray *placemarks, NSError *error) {
                            // If (lat, lng) can be reversed geocoded to something readable...
                            if (error == nil && [placemarks count] > 0) {
                                // Get placemark
                                CLPlacemark *place = [placemarks objectAtIndex:0];
                                
                                // Zoom in to new location
                                // For span: "...one degree of latitude is always approximately 111 kilometers (69 miles)" according to http://developer.apple.com/library/ios/#DOCUMENTATION/MapKit/Reference/MapKitDataTypesReference/Reference/reference.html
                                MKCoordinateSpan zoomIn = MKCoordinateSpanMake(0.03, 0.03);
                                MKCoordinateRegion region = MKCoordinateRegionMake(location, zoomIn);
                                [self.myMap setRegion:region animated:TRUE];
                                myLocText = [NSString stringWithFormat:@"Your approximate location: %@, %@, %@", place.name, [place.addressDictionary objectForKey:@"City"], [place.addressDictionary objectForKey:@"State"]];
                                [self.closestStatus setText:myLocText];
                                
                                // Get closest station
                                [self getClosestStationWithLatitude:newLocation.coordinate.latitude andLongitude:newLocation.coordinate.longitude];
                            }
                            else {
                                UIAlertView *unknown = [[UIAlertView alloc]initWithTitle:@"Unknown Location"
                                                                                  message:@"Sorry, your location could not be determined!"
                                                                                 delegate:self
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil, nil];
                                [unknown show];
                            }
                        }];
}

-(void) getClosestStationWithLatitude:(double)latitude andLongitude:(double)longitude
{
    NSString *addr = [NSString stringWithFormat:@"http://mbtamap.herokuapp.com/mapper/find_closest_stations.json?lat=%f&lon=%f",latitude, longitude];
    NSURL *url = [NSURL URLWithString:addr];
    
    // Use a concurrent dispatch queue
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData* data = [NSData dataWithContentsOfURL:url];
        [self performSelectorOnMainThread:@selector(parseData:)
                               withObject:data
                            waitUntilDone:YES];
    });
}

-(void) parseData:(NSData *)responseData
{
    NSError* error;
    NSString *label;
    
    if (responseData != nil) {
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseData
                                                    options:0
                                                      error:&error];

        if (error == nil) {
            if ([json count] > 0) {
                // Because this method is called a number of times, need to clear closestStations
                [closestStations removeAllObjects];
                
                // Iterate through JSON
                NSEnumerator *it = [json objectEnumerator];
                NSMutableArray *check = [[NSMutableArray alloc]init];
                NSDictionary *elem;
                
                // I don't want duplicate stations in list of closest stations!
                while (elem = [it nextObject]) {
                    NSDictionary *obj = [elem objectForKey:@"station"];
                    NSString *key = [obj objectForKey:@"stop_name"];
                    if (![check containsObject:key]) {
                        [closestStations addObject:obj];
                        [check addObject:key];
                    }
                }
                
                NSDictionary *closest = [closestStations objectAtIndex:0];
                double distance = [[closest objectForKey:@"distance"] doubleValue];
                if (distance > 1) {
                    label = [NSString stringWithFormat:@"%@\n\nThe closest station to you is %@ which is %f miles away from you.", myLocText, [closest objectForKey:@"stop_name"], distance];
                }
                else {
                    label = [NSString stringWithFormat:@"%@\n\nThe closest station to you is %@ which is %f mile away from you.", myLocText, [closest objectForKey:@"stop_name"], distance];
                }
                
                // Only show button when there are closest stations
                self.navigationItem.title = @"Your Location";
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"View Closest Stations"
                                                                                          style:UIBarButtonItemStylePlain                                                                                   target:self action:@selector(displayClosestStationsTable)];
                [self displayStationAnnotations];
            }
            else {
                label = [NSString stringWithFormat:@"%@\n\nThere is no station near where you are.", myLocText];
            }
            [self.closestStatus setText:label];
        }
    }
    [self.spinner stopAnimating];
}

-(void) displayClosestStationsTable
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle: nil];
    ClosestStationsTableViewController *tvc = (ClosestStationsTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"stationsTable"];
    tvc.stations = closestStations;
    [self.navigationController pushViewController:tvc animated:true];
}

-(void) displayStationAnnotations
{
    NSMutableArray *stationPins = [[NSMutableArray alloc] init];
    NSEnumerator *it = [closestStations objectEnumerator];
    NSDictionary *elem;
    while (elem = [it nextObject]) {
        NSString *distanceAway;
        if ([[elem objectForKey:@"distance"] floatValue] <= 1) {
            distanceAway = [NSString stringWithFormat:@"%f mile away", [[elem objectForKey:@"distance"] floatValue]];
        }
        else {
            distanceAway = [NSString stringWithFormat:@"%f miles away", [[elem objectForKey:@"distance"] floatValue]];
        }
        [stationPins addObject:[[StationAnnotation alloc]initWithCoordinates:CLLocationCoordinate2DMake([[elem objectForKey:@"stop_lat"] floatValue], [[elem objectForKey:@"stop_lon"] floatValue])
                                                                                title:[elem objectForKey:@"stop_name"]
                                                                             subTitle:distanceAway
                                                                 stationInfo:elem]];
    }
    [self.myMap addAnnotations:stationPins];
}

// For custom pins
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *result = nil;
    
    if ([annotation isKindOfClass:[StationAnnotation class]] == NO) {
        return result;
    }
    if ([myMap isEqual:self.myMap] == NO ) {
        return result;
    }
    StationAnnotation *senderAnnotation = (StationAnnotation *)annotation;
    NSString *pinResuableIdentifier = [senderAnnotation title];
    
    // Use MKAnnotationView and not MKPinAnnotationView because of the returning red push pin issue. More: http://stackoverflow.com/questions/9275959/annotation-image-is-replaced-by-redpushpin-when-long-press-on-annotation
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:pinResuableIdentifier];
    
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:senderAnnotation reuseIdentifier:pinResuableIdentifier];
        annotationView.canShowCallout = YES;
    }
    
    UIImage *pinImage = [UIImage imageNamed:@"t_marker.png"];
    if (pinImage != nil) {
        annotationView.image = pinImage;
    }
    result = annotationView;
    return result;
}

-(void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[StationAnnotation class]]) {
        StationAnnotation *clickedStation = (StationAnnotation *)view.annotation;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle: nil];
        ScheduleTableViewController *stvc = (ScheduleTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"scheduleTable"];
        stvc.stationInfo = clickedStation.stationInfo;
        [self.navigationController pushViewController:stvc animated:true];
    }
}
@end
